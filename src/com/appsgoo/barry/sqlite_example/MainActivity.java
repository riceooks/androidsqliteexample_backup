package com.appsgoo.barry.sqlite_example;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class MainActivity extends Activity {
	private SQLiteDatabase db;
	private Cursor cursor;
	private ContentValues cv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 設定布局
        setContentView(R.layout.activity_main);
        
        // 產生SQLiteDatabase
        db = this.openOrCreateDatabase("SQLiteDatabase.db", MODE_PRIVATE, null);
        
        // 刪除資料表
        db.execSQL("DROP TABLE IF EXISTS Device");
        
        // 建立資料表
        db.execSQL("CREATE TABLE IF NOT EXISTS Device (_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(100) NOT NULL, price DECIMAL(15,2))");
        
        // 寫入資料
        db.execSQL("INSERT INTO Device (name, price) VALUES ('洗衣機', 8950)");
        db.execSQL("INSERT INTO Device (name, price) VALUES ('冷氣機', 12550)");
        db.execSQL("INSERT INTO Device (name, price) VALUES ('電視機', 16800)");
        cv = new ContentValues();
        cv.put("name", "電冰箱");
        cv.put("price", 12888);
        db.insert("Device", null, cv);
        cv.clear();
        cv.put("name", "微波爐");
        cv.put("price", 5500);
        db.insert("Device", null, cv);
        
        // 讀取資料
        //cursor = db.rawQuery("SELECT * FROM Device ORDER BY _id DESC", null);
        cursor = db.query("Device", null, null, null, null, null, "_id DESC");
        // 將游標從頭至尾
        while(cursor.moveToNext()){
        	Toast.makeText(this, String.format("_id => %d, name => %s, price => %f", cursor.getInt(0), cursor.getString(1), cursor.getFloat(2)), Toast.LENGTH_SHORT).show();
        }
        
        // 更新資料
        //db.execSQL("UPDATE Device SET price=8888 WHERE name='洗衣機'");
        cv.clear();
        cv.put("price", 8888);
        db.update("Device", cv, "name='洗衣機'", null);
        // 重複查詢
        cursor.requery();
        // 游標第一順位值
        cursor.moveToFirst();
        Toast.makeText(this, String.format("_id => %d, name => %s, price => %s", cursor.getInt( cursor.getColumnIndex("_id") ), cursor.getString( cursor.getColumnIndex("name") ), cursor.getFloat( cursor.getColumnIndex("price") ) ), Toast.LENGTH_SHORT).show();
        
        // 刪除資料
        //db.execSQL("DELETE FROM Device WHERE _id = 2");
        db.delete("Device", "_id=2", null);
        // 重複查詢
        cursor.requery();
        // 將游標從頭至尾
        while(cursor.moveToNext()){
        	Toast.makeText(this, String.format("_id => %d, name => %s, price => %f", cursor.getInt(0), cursor.getString(1), cursor.getFloat(2)), Toast.LENGTH_SHORT).show();
        }

        // 關閉游標
        cursor.close();
        // 關閉資料庫
        db.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    
}
